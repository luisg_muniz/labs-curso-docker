---
title: Docker
description: Curso de Docker
permalink: /
---

# Curso de Docker

* Transparencias
  * {% attach_file {"file_name": "/slides_pdf/01-introduccion.pdf", "title":"01: Introducción"} %}
  * {% attach_file {"file_name": "/slides_pdf/02-instalacion.pdf", "title":"02: Instalación"} %}
  * {% attach_file {"file_name": "/slides_pdf/03-operaciones-basicas.pdf", "title":"03: Operaciones básicas"} %}
  * {% attach_file {"file_name": "/slides_pdf/04-dockerfile.pdf", "title":"04: Dockerfile"} %}
  * {% attach_file {"file_name": "/slides_pdf/05-docker-compose.pdf", "title":"05: Compose"} %}
  * {% attach_file {"file_name": "/slides_pdf/06-volumes.pdf", "title":"06: Volúmenes"} %}
  * {% attach_file {"file_name": "/slides_pdf/07-redes.pdf", "title":"07: Redes"} %}

* Documentación pública
  * [Docker](https://docs.docker.com/)
  * [Kubernetes](https://kubernetes.io/docs/)

* Enlaces extra (genéricos)
  * [Blog](https://www.docker.com/blog/), [foros](https://forums.docker.com/) y [canal en YouTube](https://www.youtube.com/user/dockerrun) oficiales de Docker.
  * [Foro de Docker](https://stackoverflow.com/questions/tagged/docker) en StackOverflow.

* Documentación extra
  * {TBD}
