---
title: Docker
subtitle: Introducción
---
# Qué vamos a tratar

  * Qué es
  * Qué permite
  * Por qué virtualizar


# Docker
![](attached_files/images/docker-logo.png)


# Docker

  * Virtualización mediante contenedores
  * Permite:
    - ...desarrollar
    - ...compartir
    - ...y desplegar


# Una máquina para cada servicio
![Un (pequeño) servidor para cada app](attached_files/images/tipo-2.png)


# Compartición de máquina
![Varias apps en la misma máquina](attached_files/images/tipo-2.png)


# Virtualización tipo II
![VirtualBox, VMWare Player, Virtual PC, ...](attached_files/images/tipo-2.png)


# Virtualización tipo I ("Bare Metal")
![Xen, VMWare ESX, KVM, LDOM, LPAR](attached_files/images/tipo-1.png)

::: notes
* También llamada _Bare Metal Virtualization_
* Paravirtualización: los sistemas operativos están ligeramente
modificados (algunos de sus _drivers_) para que accedan
directamente al hardware sin depender del Hipervisor
* Más detalles, en la Wikipedia: https://bit.ly/3xxSFyI
:::


# Contenedores
![Jails (FreeBSD), Zonas (Solaris), WPAR (AIX)](attached_files/images/tipo-containers.png)

::: notes
* Más detalles, en la Wikipedia: https://bit.ly/3H6kbGT
:::


# Qué soluciona
!["Pues en mi máquina funciona"](attached_files/images/side-eyeing-chloe.jpg)


# Qué permite
::: nonincremental
  * ~~"Pues en mi máquina funciona"~~ \pause$\leadsto$ Portabilidad\pause
  * Contenedores = estandarización\pause
    * ...y después, escalabilidad\pause
  * Livianos\pause
    * Staging\pause
    * CI / CD
:::


# Livianos: _fs layers_
![](attached_files/images/docker-fs-layers.png)


::: notes
* Créditos de las imágenes
  * https://www.docker.com/resources/what-container
  * https://worldvectorlogo.com/es/logo/docker
  * https://www.programering.com/a/MDMzAjMwATk.html
  * https://knowyourmeme.com/memes/side-eyeing-chloe
:::

# ¿Preguntas? {.plain}
\fin
