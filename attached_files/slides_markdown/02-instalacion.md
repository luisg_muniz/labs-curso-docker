---
title: Docker
subtitle: Instalación
---
# Instalación

  * Añadimos repositorio
  * yum install docker-ce
  * systemctl $\Bigl\{$enable$\Big|$start$\Bigr\}$ docker
  * Cómo quién debe ejecutar (~~`root`~~)


# Instalación

  * docker info
  * docker pull $\bigl[ ... \bigr]$
  * docker run


# Tareas
::: nonincremental
  * https://bit.ly/3wZi170 (Instalación y primeros lanzamientos)
  * https://bit.ly/38Y9aKX (Extras)
:::


# ¿Preguntas? {.plain}
\fin

:::notes
:::
