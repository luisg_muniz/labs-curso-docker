---
title: Docker
subtitle: Volúmenes
---
# Repaso: contenedores y capas
![](attached_files/images/docker-fs-layers.png)


# Volúmenes: necesidad
  * escrituras en fichero(s) => capa R/W del contenedor
    * ligada a **ese** contenedor
    * ligada a **esa** máquina
    * lenta (union fs)


# Alternativas
  * volúmenes (volumes)
  * montajes (bind mounts)
  * ~~tmpfs~~


# Volúmenes
```bash
$ docker volume create datos
$ docker run -v datos:/mntpt ...
```
  * `datos` es el **volumen** en el anfitrión
  * `/mntpt` es el punto de montaje en el contenedor


# Montajes
```bash
$ mkdir /dir-datos
$ docker run -v /dir-datos:/mntpt ...
```
  * `/dir-datos` es el **directorio** en el anfitrión
  * `/mntpt` es el punto de montaje en el contenedor


# Volúmenes: más detalles
  * Opciones: RW, RO, shared, nocopy, ...
  * docker volume $\Bigl\{$create$\Big|$ls$\Big|$prune$\Big|$rm$\Big|$...$\Bigr\}$
  * `/var/lib/docker/volumes/`
    * ... donde viven _realmente_ los datos
  * _Storage Drivers_
    * https://docs.docker.com/storage/storagedriver/


::: notes
:::

# ¿Preguntas? {.plain}
\fin
