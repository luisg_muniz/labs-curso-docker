---
title: Docker
subtitle: Dockerfile
---
# Dockerfile

  * Fichero con las _instrucciones_ para generar una imagen
  * Permite construir imágenes de forma _programática_


# Dockerfile

  * Dockerfile $\leadsto$ instrucciones
  * docker build $\leadsto$ compilación
  * docker run $\leadsto$ despliegue


# Dockerfile: instrucciones

  * Se parte de una imagen base
```dockerfile
- FROM ubuntu:22.04
- FROM ruby:3.0
- FROM scratch
```


# Dockerfile: ejemplo
```dockerfile
FROM ubuntu:18.04
RUN apt update
RUN apt -y install nginx
RUN echo "<marquee>Ola k ase</marquee>" > \
    /var/www/html/index.html
EXPOSE 80
CMD [ "/usr/sbin/nginx", "-g", "daemon off;" ]
```

::: notes
* Para evitar el _dependency hell_:
  * `update` sí, ~~`upgrade`~~ no
  * En vez de simplemente ~~`nginx`~~, mejor `nginx-1.24.0-1`
:::


# Dockerfile: compilación

  * docker build -t usu/img:1.0 $\Large\boldsymbol{.}$
    - usu = usuario/a de Docker Hub
    - $\Large\boldsymbol{.}$ = dónde localizar el Dockerfile
    - se va construyendo por capas (_fs layers_)

  * docker tag usu/img:1.0 usu/img:latest
    - Es una "buena costumbre"


# Dockerfile: tareas

::: nonincremental
  * https://bit.ly/3t5o5tu (básico)
  * https://bit.ly/3x066as (dæmon)
  * https://bit.ly/3N76KZd (entrypoint)
:::


# Dockerfile: sintaxis

  * Hay _muchas_ claúsulas distintas
  * https://dockr.ly/392atbO


# Dockerfile: tareas
  * https://bit.ly/3N0fobY (varios)


# ¿Preguntas? {.plain}
\fin

::: notes
:::
