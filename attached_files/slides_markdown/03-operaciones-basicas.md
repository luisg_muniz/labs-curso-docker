---
title: Docker
subtitle: Operaciones básicas
---
# Operaciones básicas

  * Imágenes
    - docker image $\bigl[ ... \bigr]$
    - docker images
  * Contenedores
    - docker container $\bigl[ ... \bigr]$
    - docker ps

::: notes
* Nomenclatura de imágenes:
  - Image ID: 12 primeros caracteres del resumen SHA-256
  - Tag: Etiqueta de versión
* Nomenclatura de contenedores
  - Container ID
  - Names: Autogenerado, o especificamos con "\--name"
* Código del generador de nombres: https://bit.ly/3xhgfyj
:::


# Operaciones básicas

  * docker system info
  * docker system df
  * docker system prune $\bigl[$--a$\bigr]$


# Operaciones básicas

  * https://hub.docker.com
  * docker search
  * docker login


# Operaciones básicas: tareas

  * https://bit.ly/48DiwWl (I, operaciones básicas)


# Operaciones básicas

  * **Inmutabilidad**
  * Sobre un _contenedor_:
    * docker commit
    * docker diff
  * Sobre una _imagen_:
    * docker history


# Operaciones básicas: tareas
::: nonincremental
  * https://bit.ly/3wUiesh (II, operaciones básicas)
  * https://bit.ly/3PLeBO5 (Extra, `docker` y `jq`)
:::


# Más operaciones
::: nonincremental
  * exec
  * start/stop/restart
  * pause/unpause
  * wait
  * ps/top/ports/stats
  * logs
  * rm/rmi
  * ...
:::


# Más operaciones: tareas

::: nonincremental
  * https://bit.ly/3M2AtB6 (III, más operaciones)
  * https://bit.ly/3GzbiW3 (IV, más operaciones)
:::


# ¿Preguntas? {.plain}
\fin

::: notes
:::
