---
title: Docker
subtitle: Compose
---
# docker compose: qué es

  * En vez de levantar los contenedores "manualmente" ...
  * ... (mediante `docker run`) ...
  * ... usamos un fichero en formato YAML
  * http://dockr.ly/2DthsJh


# docker compose: ejemplo
```yaml
version: '3.5'
services:
  Curso:
    container_name: web-1
    image: luisgmuniz/curso:latest
    ports:
      - 8080:80
  Curso_Prod:
    container_name: prod-1
    image: luisgmuniz/curso:1.3.0
    ports:
    - 80:80
```


# docker compose: instalación

  * yum install --y docker--compose--plugin
  * vi docker--compose.yml
  * docker compose up $\bigl[$--d$\bigr]$

::: notes
* Ojo, que en algunas distribuciones el nombre del paquete ha cambiado recientemente (de `docker-compose` a `docker-compose-plugin`)
:::


# docker compose: versiones

* ```yaml
version: '3.5'
...
```
* Compability Matrix
  - docker \--version
  - docker compose version


# docker compose: tareas

::: nonincremental
  * https://bit.ly/3M9BxU1 (I, primeros ejemplos)
  * https://bit.ly/3NLDqr3 (II, más ejemplos)
:::

# ¿Preguntas? {.plain}
\fin

::: notes
:::