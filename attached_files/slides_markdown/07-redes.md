---
title: Docker
subtitle: Redes
---
# Redes

* `ip address show docker0`
* `docker network ls`
  * bridge = docker0
  * host
  * none
  * ~~\--link~~ $\leadsto$ personalizada


# redes: modo "bridge"

* es la red por omisión
* los contenedores se conectan a esa red
* aisla del exterior
  - ... a no ser que usemos `-p` o `-P`


# redes: modo "host"

* expone el contenedor a la red pública
* ...compartiendo el direccionamiento del anfitrión
* `docker run --net=host ...`


# redes: modo "none"

* crea una red privada para el contenedor
* útil para plantillas


# red personalizada

* `docker network create --driver bridge interna-1`
* `docker run --net=interna-1 ...`


# opciones

* mac-address
* hostname
* dns
* ...
* https://bit.ly/4aYbZpP


::: notes
:::

# ¿Preguntas? {.plain}
\fin
